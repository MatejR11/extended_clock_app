import { useState, useEffect, useCallback } from 'react';

function Clock() {
  const [clock, setClock] = useState(new Date());
  const [name, setName] = useState('');
  const [day, setDay] = useState('');

  const changeDay = useCallback(() => {
    if (clock.getHours() >= 8 && clock.getHours() < 12) {
      setDay('morning');
    } else if (clock.getHours() >= 12 && clock.getHours() < 18) {
      setDay('afternoon');
    } else if (clock.getHours() >= 18 && clock.getHours() < 24) {
      setDay('evening');
    } else if (clock.getHours() >= 0 && clock.getHours() < 8) {
      setDay('night');
    }
  }, [setDay, clock]);

  useEffect(() => {
    let time = setInterval(() => {
      setClock(new Date());
    }, 1000);

    changeDay();

    return () => {
      clearInterval(time);
    };
  }, [clock, changeDay]);

  function handleNameChange(e) {
    setName(e.target.value);
  }

  return (
    <>
      <p>{clock.toLocaleTimeString()}</p>
      <input type="text" name="name" onChange={handleNameChange} />
      <p>
        Good {day}, {name}
      </p>
    </>
  );
}

export default Clock;
